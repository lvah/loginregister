import os

from celery import Celery
from django.conf import  settings

# 指定Django默认配置文件模块
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'loginRegister.settings')


CELERY_BROKER_URL = 'redis://:{}@{}:{}/7'.format(settings.REDIS_PASD, settings.REDIS_HOST, settings.REDIS_PORT)
CELERY_RESULT_BACKEND = 'redis://:{}@{}:{}/8'.format(settings.REDIS_PASD, settings.REDIS_HOST, settings.REDIS_PORT)
URL = 'redis://:{}@{}:{}/9'.format(settings.REDIS_PASD, settings.REDIS_HOST, settings.REDIS_PORT)


app = Celery('loginRegister', backend=CELERY_RESULT_BACKEND, broker=CELERY_BROKER_URL)

# 从django的settings.py里读取celery配置
app.config_from_object('django.conf:settings')

# Load task modules from all registered Django app configs.
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)


