from django.contrib import admin

from login.models import User, ConfirmCode

admin.site.register(User)
admin.site.register(ConfirmCode)
