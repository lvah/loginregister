from django.db import models

# Create your models here.

class User(models.Model):

    gender = (
        ('male', "男"),
        ('female', "女"),
    )

    name = models.CharField(max_length=128, unique=True, verbose_name='姓名')
    password = models.CharField(max_length=256, verbose_name='密码')
    email = models.EmailField(unique=True, verbose_name='电子邮箱')
    gender = models.CharField(max_length=32, choices=gender, default="男", verbose_name="性别")
    # 重点:auto_now_add(创建的时间)和auto_now(删改的时间)的区别
    create_time = models.DateTimeField(auto_now_add=True, verbose_name="创建时间")
    modify_time = models.DateTimeField(auto_now=True, verbose_name="修改时间")
    is_confirmed = models.BooleanField(default=False, verbose_name="是否验证")

    def __str__(self):
        return self.name

    class Meta:
        ordering = ["-create_time"]  # 新建用户显示在前面
        verbose_name = "用户"
        verbose_name_plural = "用户"

class ConfirmCode(models.Model):
    code = models.CharField(max_length=256)
    user = models.OneToOneField('User', on_delete=models.CASCADE)
    create_time = models.DateTimeField(auto_now_add=True)
    modify_time = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.user.name + ":   " + self.code

    class Meta:

        ordering = ["-create_time"]
        verbose_name = "确认码"
        verbose_name_plural = "确认码"