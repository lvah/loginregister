"""
@author: guofan
@contact:guofan@chanjet.com
@version: 1.0.0
@license: Apache Licence
@file: login_utils.py
@time: 2021/2/26 9:49 上午
"""

import hashlib
from datetime import datetime

from celery import shared_task
from django.conf import settings
from loginRegister.celery import  app

from login.models import ConfirmCode


def hash_code(s, salt='mysite'):  # 加点盐
    h = hashlib.sha256()
    s += salt
    h.update(s.encode())  # update方法只接收bytes类型
    return h.hexdigest()


def make_confirm_string(user):
    now = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    code = hash_code(user.name, now)
    ConfirmCode.objects.create(code=code, user=user)
    return code


@shared_task()
def hello():
    print('hello......')


@app.task
def send_email(email, code):
    print('send mail.........')
    from django.core.mail import EmailMultiAlternatives

    subject = '注册确认邮件'

    text_content = '''感谢注册，这里是登录注册系统网站！\
                    如果你看到这条消息，说明你的邮箱服务器不提供HTML链接功能，请联系管理员！'''
    html_content = '''
                    <p>感谢注册<a href="http://{}/confirm/?code={}" target=blank>点击验证</a>，\
                    这里是登录注册系统网站！</p>
                    <p>请点击站点链接完成注册确认！</p>
                    <p>此链接有效期为{}天！</p>
                    '''.format('127.0.0.1:8008', code, settings.CONFIRM_DAYS)


    msg = EmailMultiAlternatives(subject, text_content, settings.EMAIL_HOST_USER, [email])
    msg.attach_alternative(html_content, "text/html")
    msg.send()
